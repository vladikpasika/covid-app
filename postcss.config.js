const purgecss = require("@fullhuman/postcss-purgecss");
const path = require("path");
const plugins = [
  require("postcss-import"),
  require("tailwindcss")(path.resolve("./tailwind.config.js")),
  require("autoprefixer"),
  require("postcss-nested"),
];
if (process.env.NODE_ENV === "production") {
  plugins.push(
    purgecss({
      content: ["./src/**/*.js"],
      defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
      css: ["./src/**/*.css"],
      //whitelistPatternsChildren: [/ReactModal__Body--open/, /slider-dots/, /lazy-load/],
    }),
    require("cssnano")({
      preset: "default",
    })
  );
}

module.exports = {
  plugins,
};
