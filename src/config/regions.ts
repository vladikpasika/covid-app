const reg = `

Вінницька область — 174 випадки;
Волинська область — 71 випадок;
Дніпропетровська область — 58 випадків; 
Донецька область — 11 випадків;
Житомирська область — 74 випадки;
Закарпатська область — 82 випадки;
Запорізька область —  83 випадки;
Івано-Франківська область — 291 випадок;
Кіровоградська область — 118 випадків;
м. Київ — 416 випадків;
Київська область — 159 випадків;
Львівська область — 111 випадків; 
Луганська область — 3 випадки;
Миколаївська область — 2 випадки;
Одеська область — 63 випадки;
Полтавська область — 25 випадків;
Рівненська область — 119 випадків;
Сумська область — 73 випадки;
Тернопільська область — 243 випадки;
Харківська область — 11 випадків;
Херсонська область — 31 випадок;
Хмельницька область — 19 випадків;
Чернівецька область — 432 випадки;
Черкаська область — 99 випадків;
Чернігівська область — 9 випадків.



`;

const arr = [
  {
    region: "VI",
    date: "2020-04-12T17:07:48.235Z",
    cases: 349,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "VO",
    date: "2020-04-12T17:07:48.235Z",
    cases: 176,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "DP",
    date: "2020-04-12T17:07:48.235Z",
    cases: 150,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "DT",
    date: "2020-04-12T17:07:48.235Z",
    cases: 16,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "ZT",
    date: "2020-04-12T17:07:48.235Z",
    cases: 287,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "ZK",
    date: "2020-04-12T17:07:48.235Z",
    cases: 218,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "ZP",
    date: "2020-04-12T17:07:48.235Z",
    cases: 141,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "IF",
    date: "2020-04-12T17:07:48.235Z",
    cases: 589,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "KH",
    date: "2020-04-12T17:07:48.235Z",
    cases: 251,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "KC",
    date: "2020-04-12T17:07:48.235Z",
    cases: 989,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "KV",
    date: "2020-04-12T17:07:48.235Z",
    cases: 390,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "LV",
    date: "2020-04-12T17:07:48.235Z",
    cases: 274,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "LH",
    date: "2020-04-12T17:07:48.235Z",
    cases: 26,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "MK",
    date: "2020-04-12T17:07:48.235Z",
    cases: 82,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "MY",
    date: "2020-04-12T17:07:48.235Z",
    cases: 148,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "PL",
    date: "2020-04-12T17:07:48.235Z",
    cases: 116,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "RV",
    date: "2020-04-12T17:07:48.235Z",
    cases: 353,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "SM",
    date: "2020-04-12T17:07:48.235Z",
    cases: 86,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "TP",
    date: "2020-04-12T17:07:48.235Z",
    cases: 471,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "KK",
    date: "2020-04-12T17:07:48.235Z",
    cases: 82,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "KS",
    date: "2020-04-12T17:07:48.235Z",
    cases: 72,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "KM",
    date: "2020-04-12T17:07:48.235Z",
    cases: 54,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "CV",
    date: "2020-04-12T17:07:48.235Z",
    cases: 1053,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "CK",
    date: "2020-04-12T17:07:48.235Z",
    cases: 206,
    ill: null,
    recovered: null,
    death: null,
  },
  {
    region: "CH",
    date: "2020-04-12T17:07:48.235Z",
    cases: 13,
    ill: null,
    recovered: null,
    death: null,
  },
];

enum Regions {
  VI = "Вінницька область",
  VO = "Волинська область",
  DP = "Дніпропетровська область",
  DT = "Донецька область",
  ZT = "Житомирська область",
  ZP = "Запорізька область",
  ZK = "Закарпатська область",
  IF = "Івано-Франківська область",
  KH = "Кіровоградська область",
  KC = "м. Київ",
  KV = "Київська область",
  LV = "Львівська область",
  LH = "Луганська область",
  MK = "Миколаївська область",
  MY = "Одеська область",
  PL = "Полтавська область",
  RV = "Рівненська область",
  SM = "Сумська область",
  TP = "Тернопільська область",
  KK = "Харківська область",
  KS = "Херсонська область",
  KM = "Хмельницька область",
  CV = "Чернівецька область",
  CK = "Черкаська область",
  CH = "Чернігівська область",
  SC = "Місто Севастополь",
  KR = "Автономна республіка Крим",
}

export default Regions;

// const regArr = reg.split(";");
// const results = Object.entries(Regions).reduce((ak, [key, value]) => {
//   const result = regArr.reduce((akk, item) => {
//     if (item.includes(value)) {
//       const finded: any = item.match(/\d+/gi);
//       akk[key] = finded[0];
//     }
//     return akk;
//   }, {});
//   ak = { ...ak, ...result };
//   return ak;
// }, {});

// const builded = arr.map((item) => {
//   return { ...item, cases: Number(results[item.region]) };
// });
// console.log(JSON.stringify(builded));
