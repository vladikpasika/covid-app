import ApolloClient from "apollo-boost";
import isomorphicFetch from "isomorphic-fetch";
import getApiUrl from "../utils/getApiUrl";
import { InMemoryCache } from "apollo-cache-inmemory";

const client = new ApolloClient({
  uri: getApiUrl(),
  fetch: isomorphicFetch,
  cache: new InMemoryCache().restore(window['__APOLLO_STATE__']),
  credentials: 'same-origin'
});

export default client;
