import isomorphicFetch from "isomorphic-fetch";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import express from "express";

import getApiUrl from "../utils/getApiUrl";

export default function getServerApollo(req: express.Request) {
  return new ApolloClient({
    ssrMode: true,
    link: createHttpLink({
      uri: getApiUrl(),
      credentials: "same-origin",
      fetch: isomorphicFetch,
      headers: {
        cookie: req.header("Cookie"),
      },
    }),
    cache: new InMemoryCache(),
  });
}
