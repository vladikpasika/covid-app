import express from "express";

import buildReact from "./server/buildReact";

const server = express()
  .disable("x-powered-by")
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR!))
  .get("/*", buildReact);

export default server;
