import { Data } from "../modules/MapChart/MapChart.types";

export default ({
  width,
  map,
  height,
  data,
  date
}: {
  width: number;
  height: number;
  map: string;
  data: Data[];
  date: string
}) => ({
  chart: {
    borderWidth: 1,
    map,
    width,
    height,
  },
  exporting: {
    menuItemDefinitions: {
      downloadPNG: {
        text: "Скачати в PNG",
      },
      downloadPDF: {
        text: "Скачати в PDF",
      },
    },
    buttons: {
      contextButton: {
        menuItems: ["downloadPNG", "downloadPDF"],
      },
    },
  },

  title: {
    text: "Covid-19 в Україні",
  },

  subtitle: {
    text: `Статистика на основі звітів МОЗ України:${date}`,
  },

  legend: {
    enabled: false,
  },

  series: [
    {
      name: "Countries",
      color: "#E0E0E0",
    },
    {
      type: "mapbubble",
      name: "Кiлькість випадків",
      joinBy: ["postal-code", "code"],
      data,
      minSize: "3%",
      maxSize: "12%",
      color: "#CB2431",
      dataLabels: {
        enabled: true,
        format: "{point.value}",
      },
      tooltip: {
        pointFormat: "{point.region}: {point.value}",
      },
    },
  ],
});
