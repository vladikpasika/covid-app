export default function getUrl() {
  if (
    process.env.NODE_ENV === "production" &&
    process.env.BUILD_TARGET === "server"
  ) {
    return "http://api:3005/api";
  }
  return "http://localhost:3005/api";
}
