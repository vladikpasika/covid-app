import { Data, ResponseData } from "../modules/MapChart/MapChart.types";
import Regions from "../config/regions";

export default function buildData(
  data: ResponseData[],
  regions: typeof Regions
): Data[] {
  return data.map(({ region, cases }) => ({
    region: regions[region],
    value: cases,
    code: region,
    z: cases,
  }));
}
