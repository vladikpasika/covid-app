import React from "react";
import { hydrate } from "react-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import apollo from "./config/clientApollo";

import App from "./App";
import "./styles/tailwind.css";

hydrate(
  <ApolloProvider client={apollo}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();
}
