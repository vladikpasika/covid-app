import React from "react";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

import Chart from "./RelatedDynamic.component";

function buidReports(
  reports: number[],
  key: number = 0,
  results: number[] = []
): number[] {
  if (!reports[key + 1]) {
    return results;
  }
  results.push(reports[key + 1] - reports[key]);
  return buidReports(reports, ++key, results);
}

const GET_AGREGATED_DATA = gql`
  query {
    aggregatedDataForMainDynamicChart {
      categories
      reports {
        name
        data
      }
    }
  }
`;

function ChartContainer() {
  const { data } = useQuery(GET_AGREGATED_DATA);
  const categories =
    data &&
    data.aggregatedDataForMainDynamicChart.categories.map((item: string) => {
      const category = new Date();
      category.setTime(Number(item));
      return category.toLocaleDateString();
    });
  const reports =
    data &&
    data.aggregatedDataForMainDynamicChart.reports.map(
      (report: { data: number }) => report.data
    );
  const lastDate = categories && [...categories].pop();
  const buildedReports = reports && buidReports(reports);
  const buildedCategories = categories && [...categories].slice(1);
  return (
    <Chart
      className="h-screen overflow-hidden"
      reports={{ data: buildedReports, name: "Випадків в Україні" }}
      categories={buildedCategories}
      date={lastDate}
    />
  );
}

export default ChartContainer;
