export interface RelatedDynamicReport {
  _id: "CH";
  data: Array<{ cases: number; date: string }>;
}

export interface ChartAreaResponse {
  categories: string[];
  reports: RelatedDynamicReport[];
}

export interface ChartAreaData {
  categories: string[];
  data: [{ name: string; data: number }];
}
