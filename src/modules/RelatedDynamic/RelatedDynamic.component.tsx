import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useMeasure } from "react-use";

import { getAreaOptions } from "./RelatedDynamic.utils";

interface RelatedDynamicProps {
  className?: string;
  categories: string[];
  reports: { data: number[]; name: string };
  date: string;
}
function RelatedDynamic({
  reports,
  className,
  categories,
  date,
}: RelatedDynamicProps) {
  const [containerRef, { width, height }] = useMeasure();

  return (
    <div ref={containerRef} className={className}>
      <HighchartsReact
        highcharts={Highcharts}
        options={getAreaOptions(categories, reports, { width, height }, date)}
      />
    </div>
  );
}
RelatedDynamic.defaultProps = {
  data: [],
};

export default RelatedDynamic;
