export interface Data {
  value: number;
  code: string;
  region: string;
  z: number;
}

export interface ResponseData {
  date: Data;
  region: string;
  cases: number;
}
