import React from "react";

import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import buildData from "../../utils/buidMapData";
import Regions from "../../config/regions";
import Chart from "./MapChart.component";

const GET_REGIONS = gql`
  query regions($date: String) {
    regions(date: $date) {
      date
      region
      cases
    }
  }
`;

const GET_LAST_REPORTS_DATE = gql`
  query getLastReportsDate {
    lastReportsDate {
      date
    }
  }
`;
function ChartContainer() {
  const { data: reportsDate } = useQuery(GET_LAST_REPORTS_DATE);
  let date;
  if (reportsDate && reportsDate.lastReportsDate) {
    date = new Date();
    date.setTime(reportsDate.lastReportsDate.date);
  }
  const { data: regionsData } = useQuery(GET_REGIONS, {
    skip: !reportsDate || !reportsDate.lastReportsDate || !date,
    variables: {
      date: date && date.toISOString(),
    },
  });
  const buildedData = regionsData && buildData(regionsData.regions, Regions);
  return (
    <>
      {!!date && (
        <Chart
          data={buildedData}
          className="h-screen overflow-hidden"
          date={date && date.toLocaleDateString()}
        />
      )}
    </>
  );
}

export default ChartContainer;
