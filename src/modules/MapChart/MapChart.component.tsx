import React, { useMemo } from "react";
import Highcharts from "highcharts";
import HighchartsMap from "highcharts/modules/map";
import HighchartsExporting from "highcharts/modules/exporting";
import HighchartsReact from "highcharts-react-official";
// @ts-ignore
import map from "@highcharts/map-collection/countries/ua/ua-all.geo.json";
import { useMeasure } from "react-use";

import getOptions from "../../utils/getMapOptions";
import { Data } from "./MapChart.types";

if (typeof Highcharts === "object") {
  HighchartsMap(Highcharts);
  HighchartsExporting(Highcharts);
}

interface ChartProps {
  data: Data[];
  className: string;
  date: string;
}

function MapChart({ data, className, date }: ChartProps) {
  const [containerRef, { width, height }] = useMeasure();
  const memoizedOptions = useMemo(
    () => getOptions({ width, height, data, map, date }),
    [width, height, data, map]
  );
  return (
    <div ref={containerRef} className={className}>
      <HighchartsReact
        highcharts={Highcharts}
        options={memoizedOptions}
        constructorType="mapChart"
      />
    </div>
  );
}
MapChart.defaultProps = {
  data: [],
};

export default MapChart;
