import React from "react";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

import { Report } from "./AreaChart.types";
import Chart from "./AreaChart.component";
import reportsConstants from "../../config/regions";

const GET_AGREGATED_DATA = gql`
  query {
    aggregatedDataForDynamicChart {
      categories {
        date
      }
      reports {
        name
        data
      }
    }
  }
`;

function ChartContainer() {
  const { data } = useQuery(GET_AGREGATED_DATA);
  const categories =
    data &&
    data.aggregatedDataForDynamicChart.categories.map(
      (item: { date: string }) => {
        const category = new Date();
        category.setTime(Number(item.date));
        return category.toLocaleDateString();
      }
    );
  const reports = data && data.aggregatedDataForDynamicChart.reports;
  const buildedReports =
    reports &&
    reports.map((item: Report) => ({
      ...item,
      name: reportsConstants[item.name],
    }));
  const lastDate = categories && [...categories].pop();
  return (
    <Chart
      className="h-screen overflow-hidden"
      reports={buildedReports}
      categories={categories}
      date={lastDate}
    />
  );
}

export default ChartContainer;
