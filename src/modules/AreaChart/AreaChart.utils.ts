import { ChartAreaResponse } from "./AreaChart.types";
import { Report } from "./AreaChart.types";

export function buildChartAreaData({ categories, reports }: ChartAreaResponse) {
  const data = reports.map((item) => ({ name: item._id, data: item.data }));
  return {
    categories,
    data,
  };
}

export const getAreaOptions = (
  categories: string[],
  data: Report[],
  { width, height }: { width: number; height: number },
  date: string
) => {
  return {
    chart: {
      width,
      height,
    },
    title: {
      text: "Кількість випадків по регіонам",
    },

    exporting: {
      menuItemDefinitions: {
        downloadPNG: {
          text: "Скачати в PNG",
        },
        downloadPDF: {
          text: "Скачати в PDF",
        },
      },
      buttons: {
        contextButton: {
          menuItems: ["downloadPNG", "downloadPDF"],
        },
      },
    },
    subtitle: {
      text: `Статистика на основі звітів МОЗ України: ${date}`,
    },
    xAxis: {
      categories,
      tickmarkPlacement: "on",
      title: {
        enabled: false,
      },
    },
    yAxis: {
      title: {
        text: "Випадки",
      },
    },
    tooltip: {
      valueSuffix: " випадків",
    },
    plotOptions: {
      area: {
        stacking: "normal",
        lineColor: "#666666",
        lineWidth: 1,
        marker: {
          lineWidth: 1,
          lineColor: "#666666",
        },
      },
    },
    series: data,
  };
};
