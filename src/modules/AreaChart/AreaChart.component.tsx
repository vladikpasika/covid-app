import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useMeasure } from "react-use";

import { getAreaOptions } from "./AreaChart.utils";
import { Report } from "./AreaChart.types";

interface AreaChartProps {
  className?: string;
  categories: string[];
  reports: Report[];
  date: string;
}

function AreaChart({ reports, className, categories, date }: AreaChartProps) {
  const [containerRef, { width, height }] = useMeasure();

  return (
    <div ref={containerRef} className={className}>
      <HighchartsReact
        highcharts={Highcharts}
        options={getAreaOptions(categories, reports, { width, height }, date)}
      />
    </div>
  );
}
AreaChart.defaultProps = {
  data: [],
};

export default AreaChart;
