import { ChartAreaResponse, Report } from "./SimpleDynamic.types";

export function buildChartAreaData({ categories, reports }: ChartAreaResponse) {
  const data = reports.map((item) => ({ name: item._id, data: item.data }));
  return {
    categories,
    data,
  };
}

export const getAreaOptions = (
  categories: string[],
  data: { data: Report[]; name: string },
  { width, height }: { width: number; height: number },
  date: string
) => ({
  chart: {
    width,
    height,
  },
  title: {
    text: "Кількість випадків в Україні",
  },
  exporting: {
    menuItemDefinitions: {
      downloadPNG: {
        text: "Скачати в PNG",
      },
      downloadPDF: {
        text: "Скачати в PDF",
      },
    },
    buttons: {
      contextButton: {
        menuItems: ["downloadPNG", "downloadPDF"],
      },
    },
  },
  subtitle: {
    text: `Статистика на основі звітів МОЗ України: ${date}`,
  },
  xAxis: {
    categories,
    tickmarkPlacement: "on",
    title: {
      enabled: false,
    },
  },
  yAxis: {
    title: {
      text: "Випадки",
    },
  },
  tooltip: {
    split: true,
    valueSuffix: " випадків",
  },
  plotOptions: {
    area: {
      stacking: "normal",
      lineColor: "#666666",
      lineWidth: 1,
      marker: {
        lineWidth: 1,
        lineColor: "#666666",
      },
    },
  },
  series: [data],
});
