export interface SimpleDynamicReport {
  _id: "CH";
  data: Array<{ cases: number; date: string }>;
}

export interface ChartAreaResponse {
  categories: string[];
  reports: SimpleDynamicReport[];
}

export interface ChartAreaData {
  categories: string[];
  data: [{ name: string; data: number }];
}

export interface Report {
  name: string;
  data: number[];
}
