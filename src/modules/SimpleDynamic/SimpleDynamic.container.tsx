import React from "react";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

import Chart from "./SimpleDynamic.component";

const GET_AGREGATED_DATA = gql`
  query {
    aggregatedDataForMainDynamicChart {
      categories
      reports {
        name
        data
      }
    }
  }
`;

function ChartContainer() {
  const { data } = useQuery(GET_AGREGATED_DATA);
  const categories =
    data &&
    data.aggregatedDataForMainDynamicChart.categories.map((item: string) => {
      const category = new Date();
      category.setTime(Number(item));
      return category.toLocaleDateString();
    });
  const reports =
    data &&
    data.aggregatedDataForMainDynamicChart.reports.map(
      (report: { data: number }) => report.data
    );
  const lastDate = categories && [...categories].pop();
  return (
    <Chart
      className="h-screen overflow-hidden"
      reports={{ data: reports, name: "Випадків в Україні" }}
      categories={categories}
      date={lastDate}
    />
  );
}

export default ChartContainer;
