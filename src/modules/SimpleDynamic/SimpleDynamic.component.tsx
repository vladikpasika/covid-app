import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useMeasure } from "react-use";

import { getAreaOptions } from "./SimpleDynamic.utils";
import { Report } from "./SimpleDynamic.types";

interface SimpleDynamicProps {
  className?: string;
  categories: string[];
  reports: { data: Report[], name: string };
  date: string;
}
function SimpleDynamic({
  reports,
  className,
  categories,
  date,
}: SimpleDynamicProps) {
  const [containerRef, { width, height }] = useMeasure();

  return (
    <div ref={containerRef} className={className}>
      <HighchartsReact
        highcharts={Highcharts}
        options={getAreaOptions(categories, reports, { width, height }, date)}
      />
    </div>
  );
}
SimpleDynamic.defaultProps = {
  data: [],
};

export default SimpleDynamic;
