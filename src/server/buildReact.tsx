import express from "express";
import React from "react";
import { Helmet } from "react-helmet";

import { renderToString } from "react-dom/server";
import { ApolloProvider } from "@apollo/react-common";
import { getDataFromTree } from "@apollo/react-ssr";

import getClient from "../config/serverApollo";

import App from "../App";

const syncLoadAssets = () => {
  return require(process.env.RAZZLE_ASSETS_MANIFEST!);
};

export default async (
  req: express.Request,
  res: express.Response
): Promise<void> => {
  const client = getClient(req);
  const assets = syncLoadAssets();

  const ServerApp = (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  );
  await getDataFromTree(ServerApp);

  const markup = await renderToString(ServerApp);
  const tags = Helmet.renderStatic();
  res.send(
    `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charSet='utf-8' />
       ${tags.title.toString()}
        ${tags.meta.toString()}
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
        ${
          assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ""
        }
          ${
            process.env.NODE_ENV === "production"
              ? `<script src="${assets.client.js}" defer></script> <!-- Global site tag (gtag.js) - Google Analytics -->
              <script async src="https://www.googletagmanager.com/gtag/js?id=UA-164813393-1"></script>
              <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
              
                gtag('config', 'UA-164813393-1');
              </script>`
              : `<script src="${assets.client.js}" defer crossorigin></script>`
          }
          <script>
            window.__APOLLO_STATE__ = ${JSON.stringify(client.extract())};
            </script>
    </head>
    <body>
        <div id="root">${markup}</div>
    </body>
</html>`
  );
};
