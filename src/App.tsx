import React from "react";

import PageMetadata from "./modules/PageMetadata/PageMetadata";
import MapChart from "./modules/MapChart/MapChart.container";
import AreaChart from "./modules/AreaChart/AreaChart.container";
import SimpleDynamic from "./modules/SimpleDynamic/SimpleDynamic.container";
import RelatedDynamic from "./modules/RelatedDynamic/RelatedDynamic.container";

function App() {
  return (
    <>
      <PageMetadata
        title="Статистика захворюванності Covid-19 в Україні"
        description="Актуальна статистика захворюванності Covid-19 в Україні на основі данних МОЗ України"
      />
      
      <MapChart />
      <RelatedDynamic/>
      <SimpleDynamic />
      <AreaChart />
    </>
  );
}

export default App;
